import "./index.scss"
import sitecore from "../shared/js/global-helpers/sitecore-detect"

document.addEventListener("DOMContentLoaded", function() {
    if(sitecore.pageEditorMode()) {
        // Page editing mode (Experience Editor)
        // document.body.classList.add('page-editor'); // Add custom class in editor. //Sitecore already has a class on body in the Experience editor: 'is-experience-editing'. Use that if possible
    } else {
        // Normal / Preview mode. Init custom stuff that you don't want in the Experience Editor
        
    }
    jQuery("#main").prepend( "<div class='sitecore-dev'>DOM READY. sitecore.pageEditorMode(): "+sitecore.pageEditorMode()+"</div>");
});

