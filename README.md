# README

This project contains the HTML, CSS and JavaScript for the following Ørsted projects:

* Counter component - src/page-counter
* Sustainability graphs - src/page-sustainability-graphs

The purpose of the project is to provide a development environment for developing custom HTML modules for the projects and a build process for generating a single CSS and JavaScript file for each project for inclusion in Sitecore.

Production files are output to the `dist` folder where each project has it's own folder. The CSS and JavaScript file in a project folder should be uploaded into Sitecores media library and then be manually included on the Sitecore pages which needs them. **NB! All styling and Javascript should be kept in these files, please do not add styling or scripts to custom modules directly**

### Sitecore Detection

To prevent custom scripts and styles from breaking the Sitecore Experience Editor, use ```sitecore-detect.js``` to determine what code to run in each case. [README](src/sitecore-detect/README.md) 

### Assets

Any assets (images) needed for the project should be placed in `-/media/WWW/Images/Corp/Campaign/{project-folder}` or for video `-/media/WWW/Video/Corp/Campaign/{project-folder}`. This ensures that links to those assets are the same in the dev environment and in Sitecore. The assets need to be uplaoded to Sitecores media library (`WWW` folder exists there too).

### Shared modules

if you develop a custom module that could be used in multiple projects, you can place it in `src/shared/styles` and `src/shared/js` respectively, and import it from there. The `shared` folder also contains the basic styles used on http://orsted.com so that we have the same styling in development (in webpack.config.js both `vendor.css` and `app.css` is added to head). Make sure to keep the `vendor.css` and `app.css` files up to date by downloading the latest versions from the live orsted.com site once a year for instance. Read more under **"Emulating" the Sitecore environment**.

### Adding pages to a project

If you need multiple pages for a project you set it up in `webpack.config.js`. Add the site folder name to the `sites` array, and the path will automatically be created for each of the pages in `sites`.

### How do I get set up?

Run `nvm use`, `npm install` and then `npm run dev` and see the website at http://localhost:8080/

Make sure to use these versions:

* nvm: v12.11.0
* npm: 6.11.3


To build files for production run `npm run build`.


### Autoprefixer

Autoprefixer is used to prefix css files with vendor prefixes when needed. The configuration of targeted browsers is found in `.browserslistrc`.

See documentation for targeting different browsers at https://github.com/browserslist/browserslist#best-practices

### "Emulating" the Sitecore environment

Ørsteds website are already including a lot of different JavaScript libraries. When a library is installed on their website already, we should aim to use that library instead of including our own versions.

*NB: Whenever Ørsted updates their frontend our code might break, since our code depend on their libraries. So it might be nescessary to update our script along the way to work with the Ørsted frontend changes.*

Follow these steps to get the latest version of the Ørsted scripts:

1. Go to https://orsted.com/

2. Below the `<main id="main" role="main"></main>` element copy the four scripts; **runtime.js**, **app-print.js**, **vendor.js**, and **app.js**.

``` html
<script type="text/javascript" src="https://orstedcdn.azureedge.net/Assets_/dist/js/runtime.js?d=87cc4cec0fef721beb982bbd9652c011313ebbbdfa567143cec3867cfc442733"></script>
<script type="text/javascript" src="https://orstedcdn.azureedge.net/Assets_/dist/js/app-print.js?d=47321e79f3f2dbb29e4dd0ebc7866c819b1b536667bb06ec46673a1bc5d68791"></script>
<script type="text/javascript" src="https://orstedcdn.azureedge.net/Assets_/dist/js/vendor.js?d=419a8126d91522f0e0909553c6a8290daaa57fbd4407e05d442f39c6910e44e9"></script>
<script type="text/javascript" src="https://orstedcdn.azureedge.net/Assets_/dist/js/app.js?d=655341eac9d262d79ff8822a46a9196d725a2e050f5066a03a12c50ac2ce2ef7"></script>
```

3. Update the css in `@/src/shared/styles/orsted_com/vendor.css` and `@/src/shared/styles/orsted_com/app.css`. You can find the latest code here for [vendor.css](https://orstedcdn.azureedge.net/Assets_/dist/css/vendor.css) and [app.css](https://orstedcdn.azureedge.net/Assets_/dist/css/app.css).


4. Insert the scripts in the bottom of `index.html` in your project below the comment **Existing styles and javascript from Ørsted bundles**.

5. Now you should have the latest bundles and libraries that are used on the Ørsted sites.


### Libraries (GSAP)

If you want to use gsap, you should import the package manually in the Ørsted 'Custom script' field on pages in Sitecore, since it's not part of the Ørsted bundles.

**Example:**

``` html
<link rel="stylesheet" href="/-/media/WWW/Assets/Corp/Scripts/360-earth-experience/15index_styles.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.7.0/gsap.min.js" integrity="sha512-2fk3Q4NXPYAqIha0glLZ2nluueK43aNoxvijPf53+DgL7UW9mkN+uXc1aEmnZdkkZVvtJZltpRt+JqTWc3TS3Q==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script type="text/javascript" src="/-/media/WWW/Assets/Corp/Scripts/360-earth-experience/15index.js"></script>
```

### Who do I talk to?

* Gert (gj@molamil.com) has setup the build environment.
* Patrik (ps@molamil.com) is the technical development lead.
