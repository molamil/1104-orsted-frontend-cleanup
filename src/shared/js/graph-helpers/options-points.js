export default {
    elements: {
        point: {
            pointStyle: "circle",
            radius: 6,
            backgroundColor: "#fff",
            borderColor: "#8ecdc8",
            borderwidth: 4,
            // hoverRadius: 15,
            hitRadius: 2
        },
        line: {
            fill: false,
            tension: 0
        }
    }
};