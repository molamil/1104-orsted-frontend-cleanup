export default {
    chartTitle: "chart_title_energy_share",
    chartType: "customBar",
    hero: {
        label: "hero_0",
        positionOffset: {
            top: 0,
            right: 0.05
        }
    },
    labelY: "percent",
    labels: [
        "2006",
        "2008",
        "2010",
        "2012",
        "2014",
        "2016",
        "2018",
        "2019",
        "2020",
        ["2025", "target"]
    ],
    datasets: [
        {
            label: "green_energy",
            data: [17, 22, 30, 37, 44, 50, 75, 86, 90, 99],
            backgroundColor: "#8ecdc8",
            backgroundColorName: "mint",
            borderWidth: 0,
            barPercentage: 0.65, // 0.71 on mobile...
            categoryPercentage: 1,
        },
        {
            label: "black_energy",
            data: [83, 78, 70, 63, 56, 50, 25, 14, 10, 1],
            backgroundColor: "#644c76",
            backgroundColorName: "aubergine",
            borderWidth: 0,
            barPercentage: 0.65, // 0.71 on mobile...
            categoryPercentage: 1,
        }
    ],
    options: {
        barStacked: true,
        dataStartIndex: 1,
        rangeY: [0, 100],
        layout: {
            legend: {
                position: "left-bottom",
                listType: "row"
            }
        },
        // showTooltip: true, // default = true
    }
};
