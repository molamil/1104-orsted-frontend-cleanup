export default {
    scales: {
        xAxes: [
            {
                stacked: true
            }
        ],
        yAxes: [
            {
                stacked: true
            }
        ]
    }
}
