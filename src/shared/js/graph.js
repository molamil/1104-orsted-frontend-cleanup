/********************************************
GRAPHS

JS TEMPLATE:
import { setupGraphs } from "../shared/js/graph"
import { getLocaleString } from './graphs/locales'
import chart0 from './graphs/data01-circle'
let graphMap = {
    getLocaleString,
    graphs: [
        {
            elementId: 'graph-custom-1',
            headline: 'tab_title_0',
            charts: [chart0],
        },
    ]
}
setupGraphs({
    graphMap: graphMap,
})


HTML TEMPLATE:
<div id="graph-custom-1" class="graph-custom">
    CUSTOM GRAPHS WILL BE INJECTED HERE
</div>


REFERENCE:
https://www.chartjs.org/docs/latest/charts/

********************************************/

import _ from "lodash";

// global functions
import { language } from "./global-helpers/language";


// graph functions
import optionsStacked from "./graph-helpers/options-stacked";
import optionsDefault from "./graph-helpers/options-default";
import optionsPoints from "./graph-helpers/options-points";
import optionsXRange from "./graph-helpers/options-x-range";
import optionsYRange from "./graph-helpers/options-y-range";
import optionsLabelCallback from "./graph-helpers/options-callback";
import optionsTooltip from "./graph-helpers/options-tooltip";
import CreateCustomGraphTypes from "./graph-helpers/axis-labels-fix";
import {
    tooltipCallbackLine,
    tooltipCallbackBar
} from "./graph-helpers/tooltips";

// Styles
import "./../styles/graph.scss";

// Description
// Graph: The whole element, which can have multiple charts in tabs
// Chart: A visual chart in a tab

function setupGraphs({ graphMap = {}, onResize = false }) {
    let graphs = [];

    _.each(graphMap.graphs, (graph, index) => {
        // find the dom, where the graph should be placed
        const dom = document.getElementById(graph.elementId);
        if (dom) {
            // create the graph
            graphs.push(
                new Graph({
                    target: dom,
                    data: graph,
                    getLocaleString: graphMap.getLocaleString,
                    callbackResize: onResize
                })
            );
        }
    });
    console.log("Graphs", graphs);
    return graphs;
}

function resizeGraphs(elements) {
    // console.log("resizeGraphs: " + elements);
    if (!elements || elements.length == 0) {
        return;
    }
    _.each(elements, elm => {
        elm.resize();
    });
}

class Graph {
    // elm have to be a DOM element
    constructor({
        target = null,
        tweens = null,
        data = null,
        getLocaleString = null,
        callbackResize = () => {
            console.log("no callback");
        }
    }) {
        // get DOM target and jQuere it
        this.tweens = [];
        this.target = target;
        if (!this.target) {
            console.error("No graph found for '" + target + "'");
            return;
        }
        this.$target = jQuery(this.target);

        // replace all text placeholder with real text from the locales document
        this.getLocaleString = getLocaleString;
        this.traverse(data, value => {
            if (typeof value === "string") {
                return getLocaleString(value, language());
            }
            return value;
        });

        // clone data, so we don't overwrite the real data when animating and resetting
        this.graphData = data;
        this.originalGraphData = _.cloneDeep(data);

        // create array of animation timeouts
        this.chartAnimationTimeouts = [];

        // create graph
        this.createGraph({ target: this.target, graphData: data });

        // animate charts when they becomes visible
        this.update();
    }

    createGraph({ target, graphData }) {
        // creating a graph, that can contain multiple charts

        // create dom elements

        const title = graphData.headline;
        this.$tabsNavContainer = jQuery('<ul class="graph-tabs-custom">');
        this.$tabsContentContainer = jQuery(
            '<div class="graph-content-custom">'
        );
        this.$target.empty();
        if (title) {
            this.$target.append(`<h4>${title}</h4>`);
        }
        this.$target.append(this.$tabsNavContainer); // the buttons to switch tab
        this.$target.append(this.$tabsContentContainer); // the actual graphs

        this.tabs = [];
        this.charts = [];

        // Create 'customLine' and 'customBar' chart types.
        // The custom chart types format custom labels with callbacks - it does so by converting this ['2020','target'] to '2020 target'
        CreateCustomGraphTypes();

        // adding charts and tabs
        const charts = graphData.charts;
        const chartsLength = charts.length;
        _.each(charts, (chart, index) => {
            const createdChart = this.createChart({
                chartData: chart,
                chartIndex: index,
                options: this.getChartOptions(chart, index),
                hideTabs: chartsLength <= 1
            });
            this.charts.push(createdChart);
        });

        // Chart settings
        Chart.defaults.global.defaultFontFamily = "OrstedSansRegular,sans";

        // reset all charts
        _.each(this.charts, (chart, index) => {
            this.resetChart(index);
            this.resetLayout(index, false);
        });
    }

    createChart({ chartData, chartIndex, options, hideTabs = false }) {
        // creating a single chart inside a graph
        console.log('CREATE CHART', chartData)

        // add empty array to the animation timeout array - will be used when animating
        this.chartAnimationTimeouts.push([]);

        // create dom tab nav item
        const tabTitle = chartData.chartTitle || '';
        this.$tabItem = jQuery(
            `<li class="graph-tabs-tab-custom ${
                chartIndex === 0 ? "is-open" : ""
            }" data-graph-index="${chartIndex}">${tabTitle}</li>`
        );
        this.$tabItem.click(e => this.switchTab(e));
        this.$tabsNavContainer.append(this.$tabItem);

        if (hideTabs) {
            this.$tabItem.hide();
        }

        // create dom container for visual chart
        const contentTab = jQuery(
            `<div class="graph-content-tab-custom ${
                chartIndex === 0 ? "is-open" : ""
            }"></div>`
        );
        this.$tabsContentContainer.append(contentTab);
        // contentTab.append(chart.data.graph.element) // TODO: what is this?

        // save tabs for the switchTab function
        this.tabs.push({
            nav: this.$tabItem,
            content: contentTab
        });

        // generate axis labels
        // build dom structure of chart
        let dom = "";
        let legendItems = "";
        let chartType = chartData.chartType;


        let list = ``;
        let pos = `-pos-`;
        if (chartData.listLabels) {
            list = `list`
            pos = `-list-pos-`
        }

        const legendPosition = `legend${pos}${_.get(
            chartData,
            "options.layout.legend.position",
            "right"
        )}`;

        const listType = `${_.get(
            chartData,
            "options.layout.legend.listType",
            "column"
        )}`;

        let dotType = "dot";
        if (
            chartData.options &&
            chartData.options.layout &&
            chartData.options.layout.legend &&
            chartData.options.layout.legend.dotType
        ) {
            dotType = chartData.options.layout.legend.dotType;
        }

        switch (chartType) {
            case "customBar":
            case "customLine":
                // legend / x-axis-labels

                let chartHeader = "";
                let chartPercentage = "";
                let color;
                let icons = "";

                legendItems = "";

                _.each(chartData.datasets, (dataset, index) => {
                    if (dataset.label) {
                        color = dataset.backgroundColor;
                        if (dataset.labelColor) {
                            color = dataset.labelColor;
                        }
                        legendItems += `
                            <span class="legend-container">
                            <span class="legend-${dotType}" style="background-color:${color}"></span>
                                <span class="legend-label">${dataset.label}</span>
                            </span>`;
                    }
                    if (dataset.displayIcon) {
                        if (dataset.displayIcon.name == "orsted-logo") {
                            icons += `<div class="customLine-icon icon-${dataset.displayIcon.name}" data-set="${index}" data-position="${dataset.displayIcon.positionData}">
                            <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 70 19.18"><defs><style>.cls-1{fill:#409bd6;}</style><symbol id="glyph0-1" data-name="glyph0-1" viewBox="0 0 31.31 34.89"><path id="path13" class="cls-1" d="M15.67,0a15.29,15.29,0,0,1,2.64,30.41v4.48H13V30.41A15.29,15.29,0,0,1,15.67,0Zm0,5.81a9.29,9.29,0,0,0-9.51,9.52c0,4.76,2.75,8.39,6.87,9.48V15.72h5.28v9.09c4.08-1.09,6.85-4.72,6.85-9.48A9.29,9.29,0,0,0,15.67,5.81Z"/></symbol><symbol id="glyph0-2" data-name="glyph0-2" viewBox="0 0 12.97 20.78"><path id="path16" class="cls-1" d="M13,20.77V15.12c-2.45-.07-5.85-.81-7.36-2.35V0H0V20.69H5.61V18.44A11.67,11.67,0,0,0,13,20.77Z"/></symbol><symbol id="glyph0-3" data-name="glyph0-3" viewBox="0 0 16.41 21.36"><path id="path19" class="cls-1" d="M0,3.59,3.09,7.52A7.35,7.35,0,0,1,8.72,4.8c1.33,0,2.2.64,2.2,1.61C10.92,7.8,9.56,8.08,7,8.69Q.73,10,.72,15c0,3.59,3.4,6.36,7.76,6.36a9.94,9.94,0,0,0,7.44-3L13.2,14.64a6.9,6.9,0,0,1-4.67,1.84c-1.28,0-2.17-.51-2.17-1.36,0-1.15,1-1.35,3.89-2,3.95-.8,6.16-3.19,6.16-6.72C16.41,2.72,13.28,0,8.89,0,5.28,0,1.89,1.36,0,3.59Z"/></symbol><symbol id="glyph0-4" data-name="glyph0-4" viewBox="0 0 16.11 27.44"><path id="path22" class="cls-1" d="M16.11,1.23A12.12,12.12,0,0,0,11,0C6.75,0,3.31,2.72,3.31,8v7.88H0v5.09H3.31v6.52H9V20.92h5.49L13,15.83H9V8.67C9,6.08,10.16,4.92,12,4.92a6.27,6.27,0,0,1,2.83.8Z"/></symbol><symbol id="glyph0-5" data-name="glyph0-5" viewBox="0 0 20.77 21.44"><path id="path25" class="cls-1" d="M11.09,5.05a5.52,5.52,0,0,1,5,2.54l3.64-3.51A10.81,10.81,0,0,0,11.09,0C4.73,0,0,4.56,0,10.72A10.56,10.56,0,0,0,10.81,21.44c5.8,0,10-4.6,10-10.44a13.62,13.62,0,0,0-.13-2H5.89A5.18,5.18,0,0,1,11.09,5.05ZM6,13h9.17a4.41,4.41,0,0,1-4.36,3.61A4.85,4.85,0,0,1,6,13Z"/></symbol><symbol id="glyph0-6" data-name="glyph0-6" viewBox="0 0 21.36 30.13"><path id="path28" class="cls-1" d="M21.36,30.12V.36H15.75v1A10.17,10.17,0,0,0,10.64,0,10.41,10.41,0,0,0,0,10.72,10.47,10.47,0,0,0,10.64,21.44a10.27,10.27,0,0,0,5.11-1.36v10ZM15.75,7v7.41a5.78,5.78,0,0,1-4.44,1.87A5.41,5.41,0,0,1,5.8,10.72a5.47,5.47,0,0,1,5.51-5.67A5.39,5.39,0,0,1,15.75,7Z"/></symbol><symbol id="glyph0-7" data-name="glyph0-7" viewBox="0 0 0 0"><path id="path31" class="cls-1" d="M0,0"/></symbol></defs><title>logo</title><use id="use83" width="31.31" height="34.89" transform="matrix(0.55, 0, 0, -0.55, 0, 19.18)" xlink:href="#glyph0-1"/><use id="use87" width="12.97" height="20.78" transform="matrix(0.55, 0, 0, -0.55, 19.06, 18.9)" xlink:href="#glyph0-2"/><use id="use91" width="16.41" height="21.36" transform="matrix(0.55, 0, 0, -0.55, 26.77, 19.11)" xlink:href="#glyph0-3"/><use id="use95" width="16.11" height="27.44" transform="matrix(0.55, 0, 0, -0.55, 36.52, 19.08)" xlink:href="#glyph0-4"/><use id="use97" width="20.77" height="21.44" transform="matrix(0.55, 0, 0, -0.55, 45.59, 19.15)" xlink:href="#glyph0-5"/><use id="use101" width="21.36" height="30.13" transform="matrix(0.55, 0, 0, -0.55, 58.26, 19.15)" xlink:href="#glyph0-6"/><use id="use105" transform="matrix(0.7, 0, 0, -0.7, 80, 21.48)" xlink:href="#glyph0-7"/></svg>
                        </div>`;
                        }
                    }
                });

                //console.log("RE - legends: ", legendItems);

                let labelY = "";
                if (chartData.labelY) {
                    labelY = `<div class="chart-y-axis-label">
                            <p>${chartData.labelY}</p>
                    </div>`;
                }

                let labelX = "";
                if (chartData.labelX) {
                    labelY = `<div class="chart-x-axis-label">
                            <p>${chartData.labelX}</p>
                    </div>`;
                }

                if (chartType === "customLine" && chartData.chartLabel) {
                    chartHeader = `<div class="customLine-header">
                                    <span >${chartData.chartLabel}</span><br><br>
                                </div>`;
                }

                let counters = ``;


                let legends = `${legendItems}`;
                if (listType == `column`) {
                    legends = `<div class="legend-list">${legendItems}</div>`;
                }

                let legend = `<div class="chart-legend ${listType} ${legendPosition} ${chartType} ${chartType}-legend">
                            ${legends}
                       </div>`;

                let bottomLegend = ``;
                let topLegend = ``;

                if (
                    chartData.options &&
                    chartData.options.layout &&
                    chartData.options.layout.legend &&
                    chartData.options.layout.legend.position &&
                    chartData.options.layout.legend.position.indexOf("top") >= 0
                ) {
                    topLegend = legend;
                } else {
                    bottomLegend = legend;
                }

                if (chartType === "customLine" && icons != "") {
                    counters += `<div class="customLine-counters">${icons}</div>`;
                }

                dom = jQuery(
                    `
                    <div class="chart-root ${chartType}">` +
                    topLegend +
                    labelY +
                    chartHeader +
                    `<div class="chart-main ${chartType}">` +
                    chartPercentage +
                    counters +
                    `</div>` +
                    bottomLegend +
                    `</div>
                `
                );

                break;
                // options

                const legendHideTitle = _.get(
                        chartData,
                        "options.layout.legend.hideTitle",
                        false
                    ) ?
                    "hide" :
                    "";
                const legendHideValue = _.get(
                        chartData,
                        "options.layout.legend.hideValue",
                        false
                    ) ?
                    "hide" :
                    "";

                let counterFrom = 0;
                let counterTo = 10;
                let label;

                // legend
                legendItems = "";
                _.each(chartData.datasets, dataset => {
                    let unit = dataset.unit || "";
                    counterFrom = dataset.startData[0];
                    counterTo = dataset.data[0];
                    _.each(dataset.data, (dataEntry, index) => {

                        if (chartData.listLabels) {
                            label = chartData.listLabels[index];
                            legendItems += `<div class="legend-item legend-list">`;
                            legendItems += `<span class="legend-${dotType}" style="background-color:${dataset.backgroundColor[index]}"></span>`
                            legendItems += `<span class="legend-list-container">`
                            for (var i = 0; i < label.length; i++) {
                                legendItems += `<div class="legend-list-item">`
                                legendItems += `<div class="legend-list-label">${label[i].label}</div>`
                                legendItems += `<div class="legend-list-value">${label[i].value}</div>`
                                legendItems += `</div>`;
                            }
                            legendItems += `</span>`;
                            legendItems += `</div>`;
                        }

                        if (chartData.labels && chartData.labels[index] != "") {
                            label = chartData.labels[index];
                            legendItems += `<span class="legend-item">`;
                            if (
                                dataset.icon &&
                                dataset.icon[index] === "icon-plug"
                            ) {
                                legendItems +=
                                    `<svg style="fill:` +
                                    dataset.backgroundColor[index] +
                                    `;" version="1.0" class="legend-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 23 23" enable-background="new 0 0 23 23" xml:space="preserve">
<path d="M9.7,3.7H8V1.2c0-0.5,0.4-0.8,0.8-0.8c0.5,0,0.8,0.4,0.8,0.8L9.7,3.7L9.7,3.7z"/>
<path d="M5.2,1.2v2.5h-2V1.2c0-0.5,0.4-0.8,1-0.8C4.7,0.3,5.2,0.7,5.2,1.2"/>
<path d="M14.4,19.6c0.7-0.9,1.2-2.1,1.2-3.4V7.4c0-3.6,3-6.5,6.6-6.5v1.7c-2.7,0-4.9,2.1-4.9,4.8v8.8c0,1.7-0.6,3.3-1.6,4.5
	c-1.1,1.3-2.6,1.9-4.4,1.9h0c-1.8,0-3.3-0.7-4.4-1.9c-0.9-1-1.4-2.4-1.5-3.9c-2.5-0.5-4.3-2.6-4.3-5.1V5.1h10.7v6.5
	c0,2.7-2,4.9-4.7,5.2c0.1,1.1,0.5,2.1,1.1,2.8C9,20.5,10.1,21,11.3,21h0C12.6,21,13.7,20.5,14.4,19.6"/>
</svg>`;
                            }
                            legendItems += `<span class="legend-${dotType}" style="background-color:${dataset.backgroundColor[index]}"></span>
                        <span class="legend-value ${legendHideValue}">${dataEntry}${unit}</span>
                        <span class="legend-label">${label}</span>
                        </span>`;
                        }
                    });
                });

                let counter = ``;
                if (chartData.counter) {
                    counter = `<div class="animated-counter ${chartType}-counter" data-from="${counterFrom}" data-to="${counterTo}" data-unit-post="%" data-color="blue" data-speed="2000"
                            data-delay="700" data-decimals="0" data-ease="Power2.easeInOut">
                            <p class="hide">50%</p>
                    </div>`;
                }


                // graph
                dom = jQuery(
                    `
                    <div class="chart-root ${chartType} ${legendPosition}">
                        <div class="chart-main ${chartType}">` +
                    counter +
                    `</div>
                        <div class="chart-legend ${listType} ${chartType} ${legendPosition} ${list}">
                            <span class="chart-title ${legendHideTitle}">${tabTitle}</span>
                            ${legendItems}
                        </div>
                    </div>
                `
                );
                break;
            default:
                console.warn(
                    "chart type not recognized. Cant build dom",
                    chartType
                );
        }

        // console.log("DOM: ", dom);
        contentTab.append(dom);

        // create and add canvas
        const canvas = jQuery("<canvas></canvas>");
        let ctx = canvas[0].getContext("2d");
        const main = dom.find(".chart-main");
        main.append(canvas);

        // create tooltip: the box that explains the point you hover
        const tooltip = jQuery(`<div class="chart-tooltip"></div>`);
        dom.append(tooltip);

        // add hero: the lightblue-box in a speech bubble, that sits on top
        let heroLabel = _.get(chartData, "hero.label", "");
        let hero = "";
        if (heroLabel && heroLabel !== "") {
            hero = jQuery(
                `<div class="chart-hero-label" style="z-index: 1">${heroLabel}<div class="bubble"></div>`
            );
            dom.append(hero);
        }

        let chart = new Chart(ctx, {
            type: chartData.chartType,
            data: chartData,
            options
            // plugins: labelsFixPlugin,
        });



        return {
            element: dom,
            chart,
            chartData: chartData,
            // ctx, // redundant. Reference exists in chart?
            tooltip,
            hero,
            // onscreen: false,
            visible: false
        };
    }

    animateChartIn(index) {

        //console.log("animate", index);
        // get selected chart
        const selectedChart_clone = this.originalGraphData.charts[index];
        const selectedChart = this.charts[index];

        // console.log('animateChartIn', selectedChart)

        // Stop the charts animation at its current frame
        selectedChart.chart.stop();


        // get custom animation settings
        const customSpeed = _.get(
            selectedChart,
            "chart.options.customAnimation.speed",
            false
        );
        const customDelay = _.get(
            selectedChart,
            "chart.options.customAnimation.delay",
            false
        );
        const customEasing = _.get(
            selectedChart,
            "chart.options.customAnimation.easing",
            false
        );

        // Set animation options
        const type = selectedChart.chart.config.type;
        let stagger, delay, duration, easing;
        switch (type) {
            case "customBar":
                delay = customDelay || 150;
                duration = customSpeed || 1000;
                easing = customEasing || "easeOutQuart";
                break;
            case "customLine":
                stagger = 100;
                delay = customDelay || 0;
                duration = stagger - 15;
                easing = customEasing || "linear";
                break;
            default:
        }
        selectedChart.chart.options.animation.duration = duration;
        selectedChart.chart.options.animation.easing = easing;

        // ANIMATE CHART
        // Loop through all datasets in a chart
        _.each(selectedChart_clone.datasets, (dataset, indexDataSet) => {
            // Loop throug all datapoints in a dataset
            _.each(dataset.data, (dataPoint, indexDataPoint) => {
                // Set timeout to control timing of animation
                this.chartAnimationTimeouts[index][indexDataPoint] = setTimeout(
                    () => {
                        // Add a new point to the chart
                        switch (type) {
                            case "customBar":
                                // console.log('Update bar', indexDataSet, indexDataPoint, dataPoint)
                                selectedChart.chart.data.datasets[
                                    indexDataSet
                                ].data[indexDataPoint] = dataPoint;
                                break;
                            case "customLine":
                                console.log('Update line', indexDataSet, indexDataPoint, dataPoint)
                                for (
                                    let iUpdate = indexDataPoint; iUpdate < dataset.data.length; iUpdate += 1
                                ) {
                                    selectedChart.chart.data.datasets[
                                        indexDataSet
                                    ].data[iUpdate] = dataPoint;
                                }
                                break;
                            default:
                                console.warn(
                                    "dont know how to animate the chart",
                                    selectedChart
                                );
                        }
                        // Update the chart, so the new added point is visible
                        selectedChart.chart.update();

                        // show lightblue-hero-box when all points has animated
                        const allPointsAnimated =
                            indexDataPoint === dataset.data.length - 1 &&
                            indexDataSet === 0;
                        if (allPointsAnimated) {
                            this.showHero(index);
                        }

                        // When current timeout has finished running, set it to undefined
                        this.chartAnimationTimeouts[index][
                            indexDataPoint
                        ] = undefined;

                        this.resetLayout(index, false);
                        this.animateLayoutOverlay(1, selectedChart);

                    },
                    indexDataPoint * stagger + delay
                );
            });
        });

        // reset all other charts - then they will animate from the beginning when changing to their tab
        _.each(this.charts, (chart, chartIndex) => {
            if (chartIndex !== index) {
                this.resetChart(chartIndex);
            }
        });

    }

    animateLayoutOverlay(speed, selectedChart) {

        const that = this;
        let tween;
        if (this.tweens != null) {
            for (let i = 0; i < this.tweens.length; i++) {
                this.tweens[i].kill();
            }
        }
        this.tweens = [];

        const chartType = selectedChart.chartData.chartType;

        let counters,
            firstConnector,
            secondConnector,
            lastConnector;

        if (selectedChart) {
            firstConnector = selectedChart.element.find(".firstConnector");
            secondConnector = selectedChart.element.find(".secondConnector");
            lastConnector = selectedChart.element.find(".lastConnector");
        }

        if (chartType === "customLine") {
            counters = selectedChart.element.find(".customLine-counters");
            if (counters) {
                counters.css("opacity", 0);
                tween = TweenMax.to([counters], speed, {
                    opacity: 1,
                    delay: speed * 2
                });
                that.tweens.push(tween);
            }
        }
    }


    resetLayout(index, isResize) {
        if (index == null) {
            index = 0;
        }

        // Get chart
        const selectedChart = this.charts[index];
        const selectedChartData = selectedChart.chartData;
        const selectedChartData_clone = _.cloneDeep(selectedChartData);
        const chartType = selectedChart.chartData.chartType;
        const total = selectedChart.chartData.datasets[0].data.length;
        const area = selectedChart.chart.chartArea;

        // console.log("RE - chartType: ", chartType);

        let model,
            counters,
            firstConnector,
            secondConnector,
            lastConnector,
            icon;
     

        let rangeX,
            rangeY,
            w,
            h = 0;

        if (
            selectedChart.chartData.options &&
            selectedChart.chartData.options.rangeX
        ) {
            rangeX = selectedChart.chartData.options.rangeX;
            w = (rangeX[0] - rangeX[1]);
            console.log(w)
            console.log(rangeX)
        }

        if (
            selectedChart.chartData.options &&
            selectedChart.chartData.options.rangeY
        ) {
            rangeY = selectedChart.chartData.options.rangeY;
            h = rangeY[1] - rangeY[0];
        }

        let bottomLine = selectedChart.element.find(".bottomLine");
        if (bottomLine) {
            bottomLine.css("top", area.bottom - area.top);
        }

        if (selectedChart) {
            firstConnector = selectedChart.element.find(".firstConnector");
            secondConnector = selectedChart.element.find(".secondConnector");
            lastConnector = selectedChart.element.find(".lastConnector");
        }

        if (chartType === "customLine") {
            counters = selectedChart.element.find(".customLine-counters");
            icon = counters.find(".customLine-icon");
            if (icon.attr("data-set")) {
                // console.log("RE - icon: ", icon.attr("data-set"));
                model = selectedChart.chart.getDatasetMeta(
                    icon.attr("data-set")
                ).data[icon.attr("data-position")]._model;
                icon.css("top", model.y);
                icon.css("left", model.x);
                counters.css("opacity", 0);
            }
        }


        if (counters) {
            // counters.css("width", area.right - area.left);
            counters.css("height", area.bottom - area.top);
            counters.css("top", Math.floor(area.top));
        }


        if (isResize) {
            this.animateLayoutOverlay(0, selectedChart);
        }
    }

    resetChart(index) {
        // console.log("reset", index);

        if (index == null) {
            index = 0;
        }

        // Get chart
        const selectedChart = this.charts[index];
        const selectedChartData = selectedChart.chartData;
        const selectedChartData_clone = _.cloneDeep(selectedChartData);


        // Stop current animation (if any) by clearing animation timeout
        if (this.chartAnimationTimeouts[index]) {
            const length = this.chartAnimationTimeouts[index].length;
            for (let i = 0; i < length; i += 1) {
                if (this.chartAnimationTimeouts[index][i]) {
                    clearTimeout(this.chartAnimationTimeouts[index][i]);
                    this.chartAnimationTimeouts[index][i] = undefined;
                }
            }
        }
        // Stop the charts animation at its current frame
        selectedChart.chart.stop();

        // Hide all lightblue-hero-box
        this.showHero(index, false);

        // Save type and animation options
        const type = selectedChart.chart.config.type;
        const duration = selectedChart.chart.options.animation.duration;
        const dataStartIndex = _.get(
            selectedChart,
            "chartData.options.dataStartIndex",
            0
        );

        // Set animation duration to 0 to hide when the chart animates back to the init position
        selectedChart.chart.options.animation.duration = 0;

        // DATA DESCRIPTION FOR BARS
        // dataPoint = x-axis value (fx year)
        // dataSet = color of bars and their value (fx % offshore wind, % coal, % gas, % oil)

        // RESET DATA
        // Loop through all datasets in a chart
        _.each(selectedChartData_clone.datasets, (dataset, indexDataSet) => {
            // Loop throug all datapoints in a dataset
            _.each(dataset.data, (dataPoint, indexDataPoint) => {
                let newDataPoint;
                // Add a new point to the chart
                switch (type) {
                    case "customBar":
                        // set one of the bars to 100% height, and the remaining to 0% height
                        newDataPoint =
                            indexDataSet === dataStartIndex ? 100 : 0;
                        break;
                    case "customLine":
                        // set all dataPoints equal to first data point
                        newDataPoint = dataset.data[0];
                        break;
                    default:
                        newDataPoint = 0;
                }
                // Set the chart datapoint to the newDataPoint
                selectedChart.chart.data.datasets[indexDataSet].data[
                    indexDataPoint
                ] = newDataPoint;
            });
        });
        // Update the chart, so the new added point are visible
        selectedChart.chart.update();

        // Set animation duration back to default
        selectedChart.chart.options.animation.duration = duration;
    }

    update(dt) {
        // run this function on requestAnimationFrame
        window.requestAnimationFrame(this.update.bind(this));

        // for each chart, check if they are visible and animate or reset
        _.each(this.charts, (chart, index) => {
            const visible = chart.visible;
            const chartRect = chart.element[0].getBoundingClientRect();

            // reset chart, if it's set to visible, but it's actually hidden
            if (chartRect.height === 0 && visible) {
                chart.visible = false;
                this.resetChart(index);
                this.resetLayout(index, false);
                return;
            }

            // calculate if chart-content is visible
            const top = Math.max(chartRect.top, 0);
            const bottom = Math.min(chartRect.bottom, window.innerHeight);
            const height = bottom - top;
            const pct = height / chartRect.height;

            const cutoffShow = 0.65;
            const cutoffHide = 0.0;

            if (!visible && pct > cutoffShow) {
                // animate chart if it's visible
                chart.visible = true;
                this.animateChartIn(index);
            } else if (visible && pct <= cutoffHide) {
                // reset chart if it's not visible
                chart.visible = false;
                this.resetChart(index);
                this.resetLayout(index, false);
            }
        });
    }

    getChartOptions(chartData, index) {
        // POSSIBLE OPTIONS
        // options: {
        //     barStacked: true, // for bars only - you MUST to specify rangeY, when stacked is true
        //     dataStartIndex: 4 // for bars only - specify which datapoint should be 100%, before the bars animate. default=0
        //     showTooltip: true, // the box that shows on hover
        //     rangeX: [2006, 2020],
        //     rangeY: [0, 500],
        // }
        // global options
        const rangeX = _.get(chartData, "options.rangeX");
        const rangeY = _.get(chartData, "options.rangeY");
        const showTooltip = _.get(chartData, "options.showTooltip", true);
        const type = chartData.chartType;

        // chartType specific options
        let barStacked, tooltipCallback;
        switch (type) {
            case "customBar":
                barStacked = _.get(chartData, "options.barStacked", true);
                tooltipCallback = tooltipItem =>
                    tooltipCallbackBar(
                        tooltipItem,
                        this.charts[index],
                        chartData,
                        this.getLocaleString
                    );
                break;

            case "customLine":
                tooltipCallback = tooltipItem =>
                    tooltipCallbackLine(
                        tooltipItem,
                        this.charts[index],
                        chartData,
                        this.getLocaleString
                    );
                break;
            default:
        }

        // options
        let options = optionsDefault;

        let bar_points = _.merge({}, optionsPoints, chartData.options);

        const bar_options = _.merge({},
            options,
            chartData.options,
            type === "customLine" ?
            bar_points : {},
            barStacked ? optionsStacked : {},
            rangeX ? optionsXRange(rangeX[0], rangeX[1]) : {},
            rangeY ? optionsYRange(rangeY[0], rangeY[1]) : {},
            // optionsLabelCallback is for adding 'target' after some year labels
            optionsLabelCallback((datalabel, index) => {
                return chartData.labels[index];
            }),
            showTooltip ?
            optionsTooltip(tooltipItem => tooltipCallback(tooltipItem)) : {}
        );


        // return options object
        switch (type) {
            case "customBar":
            case "customLine":
            default:
                return bar_options;
        }
    }

    /* TAB NAVIGATION */
    switchTab(e) {
        let targetIndex = parseInt(e.target.dataset.graphIndex);
        if (isNaN(targetIndex)) {
            // set to first tab
            console.log("NaN", targetIndex);
        }

        // check if target is already open
        if (jQuery(e.target).hasClass("is-open")) {
            return;
        }

        // Toggle tabs
        const $ul = jQuery(e.target).closest("ul");
        let childCount = $ul.children().length;
        $ul.children().each(function(index, element) {
            if (isNaN(targetIndex)) {
                targetIndex = parseInt(jQuery(element).data("graph-index"));
                console.log("resetting to something valid...", targetIndex);
            }
            const active = index === targetIndex % childCount;
            if (active) {
                jQuery(element).addClass("is-open");
            } else {
                jQuery(element).removeClass("is-open");
            }
        });
        const contentWrapper = jQuery(e.target)
            .closest("div")
            .find(".graph-content-custom");

        // Toggle content blocks (chart)
        childCount = contentWrapper.children().length;
        contentWrapper.children().each(function(index, element) {
            const active = index === targetIndex % childCount;

            if (active) {
                jQuery(element).addClass("is-open");
            } else {
                jQuery(element).removeClass("is-open");
            }
            // no motion, just fade
            const duration = 0.5;
            TweenMax.set(element, { opacity: active ? 0.0 : 1.0 });
            TweenMax.to(element, duration, { opacity: active ? 1.0 : 0.0 });
        });
    }

    /* HERO BLUE BOX */
    buildHero(index) {
        // make hero box visible to calculate sizes
        this.charts[index].hero.css({ display: "block" });
        const bubble = this.charts[index].hero.find(".bubble");
        const bubbleRect = bubble[0].getBoundingClientRect();
        const size = bubbleRect.height * 0.5;
        this.charts[index].hero
            .find(".bubble")
            .css({ width: size, left: -Math.floor(size) + 1 });

        // console.log('build hero', index, size)

        // set position
        this.heroPosition(index);

        // hide hero box again
        this.charts[index].hero.css({ display: "" });
    }
    showHero(index, show = true) {
        // console.log('show hero')
        const hero = this.charts[index].hero;
        if (hero && show) {
            this.buildHero(index);
            // console.log('this.charts[index].hero', this.charts[index].hero)
            hero.addClass("show");
        } else if (hero) {
            hero.removeClass("show");
        }
    }
    heroPosition(index) {
        // POSSIBLE OPTIONS
        // hero: {
        //     label: 'hero_3',
        //     positionOffset: {
        //         top: 0,
        //         right: 0,
        //     },
        // },

        const selectedChart = this.charts[index];
        const heroRect = selectedChart.hero[0].getBoundingClientRect();
        const heroWidth = heroRect.width;
        const chartWidth = selectedChart.element.width();
        const chartHeight = selectedChart.element.height();
        let top = 0;
        let right = 0;

        const positionOffsetTop = _.get(
            selectedChart,
            "chartData.hero.positionOffset.top",
            0
        );
        const positionOffsetRight = _.get(
            selectedChart,
            "chartData.hero.positionOffset.right",
            0
        );

        top = chartHeight * positionOffsetTop;
        right = chartWidth * positionOffsetRight;

        this.charts[index].hero.css({ top, right });

        // TODO: CHECK THAT POSITION IS RESPONSIVE
    }

    /* EVENTS */
    resize() {
        this.resetLayout(null, true);
    }

    /* HELPERS */
    // https://gist.github.com/tushariscoolster/567c1d22ca8d5498cbc0
    traverse(obj, modifier) {
        for (let k in obj) {
            if (obj[k] && typeof obj[k] === "object") {
                this.traverse(obj[k], modifier);
            } else {
                obj[k] = modifier(obj[k]);
            }
        }
    }
}

export { setupGraphs, resizeGraphs };
