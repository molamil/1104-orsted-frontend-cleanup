import "./index.scss"
import _ from 'lodash'
import domready from 'domready'
import sitecore from "../shared/js/global-helpers/sitecore-detect"

// import components
import { setupGraphs } from "../shared/js/graph"

// import graph data specific for this page
import { getLocaleString } from './graphs/locales'
import chart0 from './graphs/data01-energy-share'
import chart1 from './graphs/data02-energy-source'
import chart2 from './graphs/data03-c02-relative'
import chart3 from './graphs/data04-c02-absolute'
import chart4 from './graphs/data05-avoided-emission'

domready(function () {
    if (sitecore.pageEditorMode()) {
        // Experience Editor, don't add a lot of custom things or move dom elements around
        return
    }

    let graphMap = {
        getLocaleString,
        graphs: [
            {
                elementId: 'graph-custom-1',
                headline: 'tab_title_0',
                charts: [chart0, chart1],
            },
            {
                elementId: 'graph-custom-2',
                headline: 'tab_title_1',
                charts: [chart2, chart3],
            },
            {
                elementId: 'graph-custom-3',
                headline: 'tab_title_2',
                charts: [chart4],
            }
        ]
    }
    setupGraphs({
        graphMap: graphMap,
    })
})
