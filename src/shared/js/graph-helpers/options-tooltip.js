const tooltip = function(callback) {
    return {
        tooltips: {
            enabled: false,
            // mode: 'index',
            // intersect: false,
            custom: callback
        },
    }
}

export default tooltip
