const callbacks = function(xCallback, yCallback) {
    const result = {
        scales: {
            xAxes: [
                {
                    ticks: {
                    }
                }
            ],
            yAxes: [
                {
                    ticks: {
                    }
                }
            ],
        },
    }
    if(xCallback) {
        result.scales.xAxes[0].ticks.callback = xCallback
    }
    if(yCallback) {
        result.scales.yAxes[0].ticks.callback = yCallback
    }
    return result
}

export default callbacks
