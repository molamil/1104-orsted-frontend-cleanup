const pageEditorMode = function() {
    if(window.Sitecore === undefined) {
        return false;
    }
    return !!(Sitecore && Sitecore.PageModes && Sitecore.PageModes.PageEditor);
};

export default {
    pageEditorMode
}
