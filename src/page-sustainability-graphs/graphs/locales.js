const strings = {
    orsted: {
        en: "Ørsted",
        da: "Ørsted",
        sv: "Ørsted",
    },
    target: {
        en: "target",
        da: "mål",
        sv: "mål",
    },
    black_energy: {
        en: "Fossil-based energy",
        da: "Sort energi",
        sv: "Fossil-baserad energi",
    },
    green_energy: {
        en: "Renewable energy",
        da: "Grøn energi",
        sv: "Förnybart avfall",
    },
    biomass: {
        en: "Sustainable biomass",
        da: "Bæredygtig biomasse",
        sv: "Hållbar biomassa",
    },
    offshore_wind: {
        en: "Offshore wind",
        da: "Havvind",
        sv: "Havsbaserad vindkraft",
    },
    onshore_wind: {
        en: "Onshore wind",
        da: "Landvind",
        sv: "Landbaserad vindkraft",
    },
    hydro: {
        en: "Hydro",
        da: "hydro",
        sv: "Vattenkraft",
    },
    coal: {
        en: "Coal",
        da: "Kul",
        sv: "Kol",
    },
    gas: {
        en: "Gas",
        da: "Gas",
        sv: "Gas",
    },
    oil: {
        en: "Oil",
        da: "Olie",
        sv: "Olja",
    },
    fossil_waste: {
        en: "Fossil waste",
        da: "Affald fra fossile brændsler",
        sv: "Fossilt avfall",
    },
    renewable_waste: {
        en: "Renewable waste",
        da: "Affald fra vedvarende energikilder",
        sv: "Förnybar energi",
    },
    actual_emissions: {
        en: "Actual emissions (million tonnes of CO<sub>2</sub>e)",
        da: "Faktiske udledninger (millioner tons CO<sub>2</sub>e)",
        sv: "Faktiska utsläpp (miljoner ton CO<sub>2</sub>e)",
    },
    relative_emissions: {
        en: "g CO<sub>2</sub>e/kWh",
        da: "g CO<sub>2</sub>e/kWh",
        sv: "g CO<sub>2</sub>e/kWh",
    },
    percent: {
        en: "Percent %",
        da: "Procent %",
        sv: "Procent %",
    },
    axis_y__avoided_emissions: {
        en: "Annual avoided emissions (million tonnes of CO<sub>2</sub>e)",
        da: "Årlig fortrængt CO<sub>2</sub>-udledning (millioner tons CO<sub>2</sub>e)",
        sv: "Utsläpp som undvikits årligen (miljoner ton CO<sub>2</sub>e)",
    },
    relative_emissions_title: {
        en: "Our CO<sub>2</sub> reduction",
        da: "Vores CO<sub>2</sub>-reduktion",
        sv: "Vår koldioxidminskning",
    },
    chart_title_energy_share: {
        en: "Green energy share in power and heat production",
        da: "Andel grøn energi i vores samlede energiproduktion",
        sv: "Andel grön energi i kraft- och värmeproduktion",
    },
    chart_title_energy_source: {
        en: "Energy sources in power and heat production",
        da: "Energikilder i el- og varmeproduktion",
        sv: "Energikällor i kraft- och värmeproduktion",
    },
    chart_title_relative_emission: {
        en: "In g CO<sub>2</sub>e/kWh",
        da: "I g CO<sub>2</sub>e/kWh",
        sv: "I g CO<sub>2</sub>e/kWh",
    },
    chart_title_absolute_emission: {
        en: "In tonnes",
        da: "I ton",
        sv: "I ton",
    },
    chart_title_avoided_emission: {
        en: "Our avoided CO<sub>2</sub> emissions from offshore wind farms",
        da: "Vores fortrængte CO<sub>2</sub>-udledninger fra havvindmølleparker",
        sv: "Koldioxidutsläpp vi har undvikit genom havsbaserad vindkraft",
    },
    tab_title_0: {
        en: "Green share of power",
        da: "Andel grøn energi",
        sv: "Andel grön energi",
    },
    tab_title_1: {
        en: "Our CO<sub>2</sub> reduction",
        da: "Vores CO<sub>2</sub>-reduktion",
        sv: "Vår koldioxidminskning",
    },
    tab_title_2: {
        en: "Our avoided CO<sub>2</sub> emissions from offshore wind farms",
        da: "Vores fortrængte CO<sub>2</sub>-udledninger fra havvindmølleparker",
        sv: "Koldioxidutsläpp vi har undvikit genom havsbaserad vindkraft",
    },
    hero_0: {
        en: "Our target is<br/>99% green energy<br/>generation by 2025",
        da: "Vores mål er 99 %<br/>grøn energiproduktion<br/>i 2025",
        sv: "Vårt mål är att generera<br/>99% grön energi 2025",
    },
    hero_1: {
        en: "Renewables now make<br/>up 90% of our<br/>energy generation",
        da: "Vedvarende energi<br/>udgør nu 90 % af<br/>vores energiproduktion",
        sv: "Förnybara produkter utgör nu<br/>90% av vår<br/>energiproduktion",
    },
    hero_2: {
        en: "Ørsted will be carbon<br/>neutral in energy generation<br/>and operations in 2025.",
        da: "Udledningen af CO<sub>2</sub> per<br/>kWh er faldet med<br/>86% siden 2006.",
        sv: "Ørsted kommer att vara koldioxidneutrala<br/>i energiproduktionen<br/>och verksamheten 2025.",
    },
    hero_3: {
        en: "We reduce our carbon<br/>emissions by phasing out coal<br/>and gas in our energy generation",
        da: "Vi reducerer vores<br/>CO<sub>2</sub>-ledning ved at udfase<br/>kul og gas i vores energiproduktion",
        sv: "Vi har minskat våra koldioxidutsläpp<br/>genom utfasning av kol och gas<br/>i vår energiproduktion",
    },
    hero_4: {
        en: "We avoid carbon emissions through<br/>our offshore wind farms.",
        da: "Vi fortrænger CO<sub>2</sub>-udledning ved<br/>hjælp af vores havvindmølleparker.",
        sv: "Vi undviker koldioxidutsläpp<br/>genom våra vindkraftsparker till havs.",
    }
}

const getLocaleString = function(key, lang) {
    if (strings[key]) {
        if (strings[key][lang]) {
            return strings[key][lang]
        }
    }
    return key
}

export {
    getLocaleString
}
