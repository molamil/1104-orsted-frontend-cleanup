// CREATE DOM ELEMENT
//
//
// Settings:
//
// type = 'div' || 'span'
// parent = DOM element
// mode = string: 'prepend' || 'append'
// id = string
// classes = string
// innerHTML = string

function createDomElm({
    parent = undefined,
    type = 'div',
    mode = 'append',
    id = null,
    classes = null,
    innerHTML = null,
}) {
    if (!parent) {
        console.error('Parent element is undefined in create-dom-elm-2.js')
        return
    }
    const el = document.createElement(type)
    if (id) {
        el.id = id
    }
    if (classes) {
        jQuery(el).addClass(classes)
    }
    if (innerHTML) {
        el.innerHTML = innerHTML
    }
    jQuery(parent)[mode](el)
    return el
}

export default createDomElm