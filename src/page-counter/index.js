import "./index.scss";
import domready from "domready";
import sitecore from "../shared/js/global-helpers/sitecore-detect";

// import components
import ResizeScrollEvents from "../shared/js/global-helpers/resize-scroll-events";
import { setupCounters, resizeCounters, scrollCounters } from "../shared/js/counter";

let resizeScrollEvents = null
let counters = []

domready(function () {
    if (sitecore.pageEditorMode()) {
        // Experience Editor, don't add a lot of custom things or move dom elements around
        return;
    }

    counters = setupCounters();

    // handle resize and scroll
    resizeScrollEvents = new ResizeScrollEvents({
        scrollThrottleMillis: 0,
        onScroll: () => {
            // insert functions to run on scroll
            // console.log('index.js', 'handleScroll')
            handleScroll();
        },
        onResize: () => {
            // insert functions to run on resize
            // console.log('index.js', 'handleResize')
            handleResize();
        }
    });
});

function handleResize() {
    resizeCounters(counters)
}

function handleScroll() {
    scrollCounters(counters, resizeScrollEvents.viewport)
}

