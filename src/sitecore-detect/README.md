# Sitecore Editor Detection #

To prevent breaking the Experience Editor when adding custom scripts and styling use this component to detect when the page is loaded normally and when it is loaded in the Experience Editor.

## Javascript ##
Add ```import sitecore from "../shared/js/sitecore-detect"``` to your Javascript and then call ```sitecore.pageEditorMode()``` to check if the page is displayed in the Experience Editor. In most cases refrain from initializing custom components in the Experience Editor.

Example:
```javascript
document.addEventListener("DOMContentLoaded", function() {
    if(sitecore.pageEditorMode()) {
        // Experience Editor, don't add a lot of custom things or move dom elements around
    } else {
        // Normal / Preview mode. Init all your stuff here
    }
});
```

## Styles ##
When a page is loaded in the experience Editor a class ```is-experience-editing``` is added to the body tag. Use that to disable css in the Experience Editor. It is not necessary to include the style ```sitecore-detect.scss```, it is only used for testing this component, instead use the class ```is-experience-editing``` with your own styles.

Example:
```css
body.is-experience-editing {
    .my-custom-thing {
        display: none; /* We might not need to see everything in the experience editor */
    }
}
body:not(.is-experience-editing) {
    .my-custom-thing {
        flex-direction: row-reverse; /* Avoid moving elements around in the experience editor */
    }
}
```
