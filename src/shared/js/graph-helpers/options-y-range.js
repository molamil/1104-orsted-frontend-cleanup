const range = function(min, max, stepSize) {
    return {
        scales: {
            yAxes: [
                {
                    autoSkip: false,
                    type: 'linear',
                    ticks: {
                        min,
                        max,
                        stepSize,
                    }
                }
            ],
        },
    }
}

export default range
