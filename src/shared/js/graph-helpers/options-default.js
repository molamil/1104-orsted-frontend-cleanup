export default {
    scales: {
        xAxes: [{
            ticks: {
                autoSkip: false,
                // fontColor: "#644c76",
                fontColor: "#3B4956",
                fontFamily: "OrstedSansRegular,sans",
                fontSize: 14,
                padding: 25
            },
            gridLines: {
                display: false,
                color: "#DDDDDD"
            }
        }],
        yAxes: [{
            ticks: {
                autoSkip: false,
                // fontColor: "#644c76",
                fontColor: "#3B4956",
                fontFamily: "OrstedSansRegular,sans",
                fontSize: 14,
                padding: 15
            },
            gridLines: {
                display: false,
                drawBorder: false
            }
        }]
    },
    layout: {
        padding: {
            left: 0,
            right: 0,
            top: 0,
            bottom: 0
        }
    },
    legend: {
        display: false
    },
    hover: {
        mode: false
            // intersect: false,
            // axis:'y',
    },
    tooltips: {
        enabled: false,
        intersect: false,
        axis: "x",
        // mode: 'nearest',
        custom: function(tooltipModel) {
            // TODO: Implement custom tooltip
            // console.log('Custom tooltip', tooltipModel)
        }
    },
    animation: {
        duration: 0,
        easing: "easeOutQuart"
    },
    responsive: true,
    maintainAspectRatio: false
};