/*******************************************
RESIZE SCROLL EVENTS

* How to use
let resizeScrollEvents = null

// handle resize and scroll
resizeScrollEvents = new ResizeScrollEvents({
    resizeDebounceMillis: 250, // optional
    scrollThrottleMillis: 150, // optional
    onScroll: () => {
        handleScroll();
    },
    onResize: () => {
        handleResize();
    }
});
function handleResize() {}
function handleScroll() {}

// you can now access the viewport values like this:
const viewport = resizeScrollEvents.viewport

// so fx inside handleScroll() you can add the following, if the scroll function needs the viewport value:
scrollCounters(counters, resizeScrollEvents.viewport);




*******************************************/

import { throttle, debounce, isMobile } from './utils'

class ResizeScrollEvents {
    constructor(vars = {
        scrollThrottleMillis: 150,
        resizeDebounceMillis: 250,
        onScroll: () => console.log('on scroll'),
        onResize: () => console.log('on resize'),
    }) {
        this.onResize = vars.onResize
        this.onScroll = vars.onScroll

        // Fixed variables
        this.SCROLL_THROTTLE_MILLIS = vars.scrollThrottleMillis || 150
        this.RESIZE_DEBOUNCE_MILLIS = vars.resizeDebounceMillis || 250
        // this.maxWindowWidth = 1440

        // Dynamic variables
        this.currentWindowHeight = window.innerHeight
        this.prevWindowHeight = this.currentWindowHeight
        this.currentWindowWidth = document.body.clientWidth
        this.prevWindowWidth = this.currentWindowWidth
        this.setScrollDynamicVariables()

        this.runEventListeners()
    }

    setScrollDynamicVariables() {
        const top = window.pageYOffset | document.body.scrollTop
        const bottom = top + this.currentWindowHeight
        this.viewport = { top, bottom }
    }

    runEventListeners() {
        window.addEventListener(
            'scroll',
            throttle(() => {
                this.setScrollDynamicVariables()
                this.onScroll()
            }, this.SCROLL_THROTTLE_MILLIS)
        )
        window.addEventListener(
            'resize',
            debounce(() => {
                // Handle when top and bottom bars disappears/appears in mobile browser:
                //    If NOT mobile device, it should resize at all times.
                //    If mobile it should only resize when both width and height changes (fx when changing device orientation).
                this.currentWindowHeight = window.innerHeight
                this.currentWindowWidth = document.body.clientWidth

                if (isMobile()) {
                    if (this.currentWindowHeight !== this.prevWindowHeight && this.currentWindowWidth !== this.prevWindowWidth) {
                        // console.log('IS mobile - resizing')
                        this.setScrollDynamicVariables()
                        this.onResize()
                    }
                }
                else {
                    // console.log('NOT mobile - resizing')
                    this.setScrollDynamicVariables()
                    this.onResize()
                }
                this.prevWindowHeight = this.currentWindowHeight
                this.prevWindowWidth = this.currentWindowWidth
            }, this.RESIZE_DEBOUNCE_MILLIS)
        )
    }
}

export default ResizeScrollEvents