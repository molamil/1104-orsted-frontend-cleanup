// LabelFix init creates three new chart types; 'customLine', 'customLinePercentage' and 'customBar'
// The init function must run before creating the graphs.
// 'customLine' and 'customBar' joins multiple label strings fx ['2020', 'target'] to '2020 target'

const labelFixInit = function(chart, datasetIndex) {
    Chart.controllers.bar.prototype.initialize.call(this, chart, datasetIndex);
    const scale = this.chart.scales["x-axis-0"];
    const originalLabels = _.clone(this.chart.config.data.labels, true);

    const flatLabels = [];
    _.each(originalLabels, function(label, index) {
        // console.log(label, index)
        if (typeof label === "string") {
            flatLabels.push(label);
        } else {
            flatLabels.push(label.join(" "));
        }
    });

    const originalAfterCalculateTickRotation = scale.afterCalculateTickRotation;
    scale.afterCalculateTickRotation = function() {
        originalAfterCalculateTickRotation.apply(this);
        if (scale.labelRotation > 20) {
            this.chart.config.data.labels = _.clone(flatLabels);
        } else {
            this.chart.config.data.labels = _.clone(originalLabels);
        }
    };
};

const init = function() {
    Chart.defaults.customLinePercentage = Chart.defaults.line;
    Chart.defaults.customLine = Chart.defaults.line;
    Chart.defaults.customBar = Chart.defaults.bar;
    Chart.defaults.customBarCounters = Chart.defaults.bar;
    Chart.defaults.customBarPercentage = Chart.defaults.bar;
    Chart.defaults.customBarGap = Chart.defaults.bar;
    Chart.defaults.customBarComparison = Chart.defaults.bar;

    Chart.controllers.customLinePercentage = Chart.controllers.line.extend({
        initialize: labelFixInit
    });

    Chart.controllers.customLine = Chart.controllers.line.extend({
        initialize: labelFixInit
    });

    Chart.controllers.customBar = Chart.controllers.bar.extend({
        initialize: labelFixInit
    });

    Chart.controllers.customBarCounters = Chart.controllers.bar.extend({
        initialize: labelFixInit
    });
    Chart.controllers.customBarPercentage = Chart.controllers.bar.extend({
        initialize: labelFixInit
    });
    Chart.controllers.customBarGap = Chart.controllers.bar.extend({
        initialize: labelFixInit
    });
    Chart.controllers.customBarComparison = Chart.controllers.bar.extend({
        initialize: labelFixInit
    });
};

export default init;