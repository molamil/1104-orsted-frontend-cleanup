import { language } from "./../global-helpers/language";

// OLD STUFF. Evaluate and refactor
const toolTipContent = function(tooltipItem, data) {
    let title = tooltipItem.title.join(" "); // array to string
    title = title.split(","); // split on comma
    title = title.join(" "); // put together with space
    const htmlTitle = `<p class="title"><strong>${title}</strong></p>`;
    // find index of current toolTipContent by comparing title with dataset title
    const index = data.labels.findIndex(item => {
        if (typeof item === "string") {
            return item === tooltipItem.title.join(" ");
        } else {
            return item.join(" ") === title;
        }
    });
    
    let htmlContent = "";
    if (index !== -1) {
        data.datasets.forEach(function(dataset) {
            let color = dataset.backgroundColor || dataset.borderColor;
            htmlContent += `<p class="value"><span class="dot" style="background-color:${color}"></span><span>${dataset.label}&nbsp;</span><span><strong>${dataset.data[index]}%</strong></span></p>`;
        });
    }
    // $tooltip.html(title + htmlContent)
    return htmlTitle + htmlContent;
};

const toolTipContentLine = function(tooltipItem, data, getLocaleString) {
    const yearStringCurrent = tooltipItem.title.join(" ");
    const yearStringFirst = data.labels[0]

    const yearIntCurrent = parseInt(yearStringCurrent)
    const yearIntFirst = parseInt(yearStringFirst)

    console.log('yearIntCurrent', yearIntCurrent, 'yearIntFirst', yearIntFirst, 'tooltipItem', tooltipItem)
    const index = yearIntCurrent - yearIntFirst

    let label = data.labels[index]
    if (typeof label === "object") {
        label = label.join(' ')
    }

    let content = "";
        const dataset = data.datasets[0];
        let color = dataset.backgroundColor || dataset.borderColor;
        console.log('dataset', dataset.data, index)
        content += `
        <p class="value">
            <span class="dot" style="background-color:${color}"></span>
            <span>${getLocaleString( "orsted", language() )}&nbsp;</span>
            <span>
                <strong>${dataset.data[index].y}</strong>
            </span>
        </p>`

    return `<p class="title"><strong>${label}</strong></p>${content}`;
};

const tooltipCallbackLine = function(
    tooltipItem,
    chart,
    data,
    getLocaleString
) {
    console.log('tooltip', tooltipItem)
    const show = tooltipItem.opacity === 1.0;
    const $tooltip = jQuery(chart.tooltip);

    if (show) {
        $tooltip.html(toolTipContentLine(tooltipItem, data, getLocaleString));

        const isShowing = $tooltip.hasClass("show");
        /*
        if ($tooltip.hasClass('show')) {
            return;
        }
        */

        $tooltip.addClass("show");

        const toolTipRect = chart.tooltip[0].getBoundingClientRect();
        var canvasLeft = chart.chart.canvas.offsetLeft;

        const xPos = tooltipItem.caretX + canvasLeft - toolTipRect.width / 2;
        const yPos = tooltipItem.caretY - toolTipRect.height - 20;
        const offset = 20;

        if (!isShowing) {
            TweenMax.set(chart.tooltip, {
                x: `${xPos}px`,
                y: `${yPos + offset}px`,
                opacity: 0.0
            });
        }
        TweenMax.to(chart.tooltip, 0.35, {
            x: `${xPos}px`,
            y: `${yPos}px`,
            opacity: 1.0
        });
    } else {
        $tooltip.removeClass("show");
        TweenMax.killTweensOf(chart.tooltip);
        TweenMax.set(chart.tooltip, {
            opacity: 0.0
        });
    }
};

const tooltipCallbackBar = function(tooltipItem, chart, data) {
    // console.log('tooltipCallbackBar', tooltipItem, data)
    const show = tooltipItem.opacity === 1.0;
    const $tooltip = jQuery(chart.tooltip);

    if (show) {
        $tooltip.html(toolTipContent(tooltipItem, data));

        const isShowing = $tooltip.hasClass("show");
        /*
        if ($tooltip.hasClass('show')) {
            return;
        }
        */
        $tooltip.addClass("show");

        const toolTipRect = chart.tooltip[0].getBoundingClientRect();
        var canvasLeft = chart.chart.canvas.offsetLeft;
        const canvasRect = chart.chart.canvas.getBoundingClientRect();
        const barWidth = chart.chart.getDatasetMeta(1).data[0]._model.width;
        const pos = tooltipItem.caretX + canvasLeft;
        const isLeft = pos < canvasRect.width / 2;
        const off = barWidth / 2 + Math.sqrt(2) * 5;
        let xPos = tooltipItem.caretX + canvasLeft;
        if (isLeft) {
            $tooltip.addClass("left");
            $tooltip.removeClass("right");
            xPos += off;
        } else {
            $tooltip.addClass("right");
            $tooltip.removeClass("left");
            xPos -= toolTipRect.width + off;
        }
        const yPos = canvasRect.height / 2 - 20 - toolTipRect.height / 2;
        const offset = 20;
        // TweenMax.killTweensOf(chart.tooltip)
        if (!isShowing) {
            TweenMax.set(chart.tooltip, {
                x: `${xPos}px`,
                y: `${yPos + offset}px`,
                opacity: 0.0
            });
        }
        TweenMax.to(chart.tooltip, 0.35, {
            x: `${xPos}px`,
            y: `${yPos}px`,
            opacity: 1.0
        });
    } else {
        $tooltip.removeClass("show");
        TweenMax.killTweensOf(chart.tooltip);
        TweenMax.set(chart.tooltip, {
            opacity: 0.0
        });
    }
};

export { tooltipCallbackLine, tooltipCallbackBar };
