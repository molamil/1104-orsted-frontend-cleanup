export default {
    chartTitle: "chart_title_avoided_emission",
    chartType: "customLine",
    hero: {
        label: "hero_4",
        positionOffset: {
            top: 0.05,
            right: 0.05,
        }
    },
    labelY: "axis_y__avoided_emissions",
    labels: [
        "2006",
        "2007",
        "2008",
        "2009",
        "2010",
        "2011",
        "2012",
        "2013",
        "2014",
        "2015",
        "2016",
        "2017",
        "2018",
        "2019",
        "2020",
    ],
    datasets: [
        {
            data: [
                { x: 2006, y: 0.37 },
                { x: 2007, y: 0.36 },
                { x: 2008, y: 0.55 },
                { x: 2009, y: 0.72 },
                { x: 2010, y: 1.2 },
                { x: 2011, y: 1.48 },
                { x: 2012, y: 1.67 },
                { x: 2013, y: 2.71 },
                { x: 2014, y: 2.91 },
                { x: 2015, y: 3.42 },
                { x: 2016, y: 3.64 },
                { x: 2017, y: 5.3 },
                { x: 2018, y: 6.3 },
                { x: 2019, y: 7.6 },
                { x: 2020, y: 8.1 },
            ],
            borderColor: "#8ecdc8",
            borderColorName: "mint",
            borderWidth: 4,
            fill: false
        }
    ],
    options: {
        rangeX: [2006, 2020],
        rangeY: [0, 10],
    }
};
