/*******************************************
COUNTER
Tweens a number from A to B


HTML template - full
<div
    class="animated-counter"
    data-from="0"
    data-to="10800"
    data-unit-pre="$"
    data-unit-post="&nbsp;GW"
    data-unit-big="billion"
    data-unit-small="Expected investments in jobs created by Bay State Wind from 2015-2018"
    data-color="blue"
    data-color-unit-small="blue"
    data-speed="1500"
    data-delay="0"
    data-decimals="0"
    data-ease="Power2.easeOut"
    data-steps="0"
    data-animate-once="false"
    data-animate-number="true"
    data-english-comma="true"
    // data-font-size="160"
>
<p class="hide">10800</p>
</div>


HTML template - minimum
<div
    class="animated-counter"
    data-to="10800"
>
<p class="hide">10800</p>
</div>

*******************************************/

import createDomElm from './global-helpers/create-dom-elm-2.js'
import findAncestor from './global-helpers/find-ancestor.js'
import './../styles/counter.scss'

function setupCounters(elementClass = "animated-counter") {
    if (!elementClass) {
        console.warn("counter: No elementClass provided")
        return
    }

    let elements = document.getElementsByClassName(elementClass)
    // convert HtmlCollection to Array to 'freeze' the list of items
    elements = Array.prototype.slice.call(elements)
    console.log('COUNTER.JS')
    console.log(elements)

    const elmArray = []
    Array.prototype.forEach.call(elements, (elm, index, arr) => {
        elmArray.push(new Counter(elm))
    })
    console.log("Counters", elmArray)

    return elmArray
}

function scrollCounters(elements, viewport) {
    if (!elements || elements.length == 0) {
        return
    }
    Array.prototype.forEach.call(elements, elm => {
        elm.scroll(viewport)
    })
}

function resizeCounters(elements) {
    if (!elements || elements.length == 0) {
        return
    }
    Array.prototype.forEach.call(elements, elm => {
        elm.resize()
    })
}

class Counter {
    // elm have to be a DOM element
    constructor(elm) {
        // get DOM target and jQuere it
        this.target = elm
        if (!this.target) {
            console.error("No counter found for '" + elm + "'")
            return
        }

        this.$target = jQuery(this.target)

        // settings
        this.settingsDefault = {
            from: 0,
            to: 100,
            unitPre: "",
            unitPost: "",
            unitBig: "",
            unitSmall: "",
            color: "grey", // can be: black, grey, white, blue, green, yellow, purple, red
            colorUnitSmall: "grey",
            speed: 1000, // in milliseconds
            delay: 0, // in milliseconds
            decimals: 0,
            ease: 'Power2.easeOut', // NB.: if steps are set, ease is not used
            steps: 0, // 10, 100 ... --> rounds up, so the X most right digits stay on zero while animating
            animateOnce: false, // Whether the animation should repeat everytime the number enters the view again
            animateNumber: true,
            englishComma: true,
            // fontSize: 0, // 160, 100, ...
        }
        this.settings = {
            from: parseFloat(this.target.dataset.from) ||
                this.settingsDefault.from,
            to: parseFloat(this.target.dataset.to) || this.settingsDefault.to,
            unitPre: this.target.dataset.unitPre || this.settingsDefault.unitPre,
            unitPost: this.target.dataset.unitPost || this.settingsDefault.unitPost,
            unitBig: this.target.dataset.unitBig || this.settingsDefault.unitBig,
            unitSmall: this.target.dataset.unitSmall || this.settingsDefault.unitSmall,
            color: this.target.dataset.color || this.settingsDefault.color,
            colorUnitSmall: this.target.dataset.colorUnitSmall || this.settingsDefault.colorUnitSmall,
            speed: parseInt(this.target.dataset.speed) ||
                this.settingsDefault.speed,
            delay: parseInt(this.target.dataset.delay) ||
                this.settingsDefault.delay,
            decimals: parseInt(this.target.dataset.decimals) ||
                this.settingsDefault.decimals,
            ease: this.target.dataset.ease || this.settingsDefault.ease,
            steps: parseInt(this.target.dataset.steps) ||
                this.settingsDefault.steps,
            animateOnce: this.target.dataset.animateOnce === "true" ?
                true : this.settingsDefault.animateOnce,
            animateNumber: this.target.dataset.animateNumber === "false" ?
                false : this.settingsDefault.animateNumber,
            englishComma: this.target.dataset.englishComma === "false" ?
                false : this.settingsDefault.englishComma,
            // fontSize: parseFloat(this.target.dataset.fontSize) ||
            //     this.settingsDefault.fontSize
        }

        

        this.animateValue =
            (this.settings.to - this.settings.from) /
            (this.settings.speed / 1000)
        this.hasAnimated = false
        this.timesAnimated = 0

        // add classes
        const parentSection = findAncestor(this.target, "section")
        if (parentSection) {
            parentSection.classList.add("section-with-counter")
        }
        this.target.parentNode.classList.add("animated-counter-parent")
        this.target.classList.add("counter-" + this.settings.color)
        this.target.classList.add("counter-unit-" + this.settings.colorUnitSmall)
        if (this.settings.unitPre) {
            this.target.classList.add("hasUnitPre")
        }
        if (this.settings.unitPost) {
            this.target.classList.add("hasUnitPost")
        }

        this.numberContainer = createDomElm({
            parent: this.target,
            classes: "numberContainer"
        })

        const numberDom = createDomElm({
            parent: this.numberContainer,
            classes: "number",
            innerHTML: this.settings.animateNumber ?
                String(this.settings.from) : String(this.settings.to)
        })


        // create DOM elements
        this.dom = {
            unitPre: this.settings.unitPre ?
                createDomElm({
                    parent: this.numberContainer,
                    classes: "unitPre",
                    mode: 'prepend',
                    innerHTML: String(this.settings.unitPre)
                }) : null,
            number: numberDom,
            unitPost: this.settings.unitPost ?
                createDomElm({
                    parent: this.numberContainer,
                    classes: "unitPost",
                    innerHTML: String(this.settings.unitPost)
                }) : null,
            unitBig: this.settings.unitBig ?
                createDomElm({
                    parent: this.target,
                    classes: "unitBig",
                    innerHTML: String(this.settings.unitBig)
                }) : null,
            unitSmall: this.settings.unitSmall ?
                createDomElm({
                    parent: this.target,
                    classes: "unitSmall",
                    innerHTML: String(this.settings.unitSmall)
                }) : null
        }


        // if (this.settings.fontSize != 0) {
        //     numberDom.style.fontSize = this.settings.fontSize + "px"
        //     this.dom.unitPre.style.fontSize = this.settings.fontSize * 0.625 + "px"
        //     this.dom.unitPost.style.fontSize = this.settings.fontSize * 0.625 + "px"
        // }

        // set ease
        this.ease = this.settings.ease
        // if steps are set, we use that in stead of ease
        if (this.settings.steps && this.settings.steps > 0) {
            this.ease = SteppedEase.config(
                (this.settings.to - this.settings.from) / this.settings.steps
            )
        }

        this.prevInView = undefined

        // get target position
        // console.log('initial resize')
        this.resize()

        if (this.settings.animateNumber) {
            // reset number to 'from'
            this.reset()
            // intro animate all in (otherwise the animations will only trigger on scroll)
            this.animate()
        } else {
            // reset number to 'to'
            this.reset(this.settings.to)
            // don't animate
        }

        // hide placeholder text
        this.$target.find(".hide").hide()

        // click event - for testing
        // this.target.addEventListener('click', () => {
        //     this.reset()
        //     this.animate()
        // })
        setTimeout(() => {
            // if other things are resizing with js, wait a bit before resizing these things
            this.resize()
        }, 100)
    }

    animate() {
        this.settings.to = parseFloat(this.target.dataset.to) || this.settingsDefault.to
        // tween number
        this.tween = window.gsap.to(this.count, this.settings.speed / 1000, {
            var: this.settings.to,
            ease: this.ease,
            delay: this.settings.delay / 1000,
            onUpdate: this.updateDom,
            onUpdateParams: [this, this.count, "var"]
        })
    }
    reset(resetToNumber = this.settings.from) {
        // kill existing tweens (if any)
        if (this.tween) {
            window.gsap.killTweensOf(this.count)
            this.tween = null
        }
        // reset number to 'from'
        this.count = {
            var: resetToNumber
        }
        // update dom
        this.updateDom(this, this.count, "var")
    }
    updateDom(that, obj, prop) {
        let newNumber = obj[prop].toFixed(that.settings.decimals).toString()
        if (!that.settings.englishComma) {
            newNumber = newNumber.replace(/\./g, ",")
        }
        newNumber = newNumber.replace(
            /\B(?=(?:\d{3})+(?!\d))/g, !that.settings.englishComma ? "." : ","
        )
        that.dom.number.innerText = newNumber
    }
    resize() {
        // get target position
        this.elementTop = this.$target.offset().top
        this.elementBottom = this.elementTop + this.$target.outerHeight()
        // console.log('Counter resize', this.elementTop, this.elementBottom)
    }
    scroll(viewport) {
        const isInView =
            this.elementBottom > viewport.top &&
            this.elementTop < viewport.bottom
        this.inView = isInView
    }

    // Play the animation from outside this class object
    set inView(bool) {
        if (bool === this.prevInView) return

        // when this value is changed, we animate / reset
        if (this.settings.animateNumber) {
            if (bool) {
                if (!this.hasAnimated) {
                    this.animate()
                    this.hasAnimated = true
                    this.timesAnimated++
                }
            } else {
                // If it's set to repeat animations or if it has only done one intro animation
                if (!this.settings.animateOnce || this.timesAnimated < 1) {
                    this.reset()
                    this.hasAnimated = false
                }
            }
        }
        this.prevInView = bool
        return bool
    }
}

export { setupCounters, resizeCounters, scrollCounters, Counter }
