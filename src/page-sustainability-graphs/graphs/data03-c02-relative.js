export default {
    chartTitle: "chart_title_relative_emission",
    chartType: "customLine",
    hero: {
        label: "hero_2"
    },
    labelY: "relative_emissions",
    labels: [
        "2006",
        "2007",
        "2008",
        "2009",
        "2010",
        "2011",
        "2012",
        "2013",
        "2014",
        "2015",
        "2016",
        "2017",
        "2018",
        "2019",
        "2020",
        ["2023", "target"],
        ["2025", "target"],
    ],
    datasets: [
        {
            borderDash: [5, 5],
            data: [
                { x: 2006, y: 462 },
                { x: 2007, y: 424 },
                { x: 2008, y: 406 },
                { x: 2009, y: 393 },
                { x: 2010, y: 341 },
                { x: 2011, y: 337 },
                { x: 2012, y: 282 },
                { x: 2013, y: 311 },
                { x: 2014, y: 280 },
                { x: 2015, y: 220 },
                { x: 2016, y: 224 },
                { x: 2017, y: 151 },
                { x: 2018, y: 131 },
                { x: 2019, y: 65 },
                { x: 2020, y: 58 },
                { x: 2021, y: 20 }, // maps to '2023 target'
                { x: 2022, y: 10 }, // maps to '2025 target'
            ],
            borderColor: "#8ecdc8",
            borderColorName: "mint",
            borderWidth: 4,
            fill: false
        },
        {
            data: [
                { x: 2006, y: 462 },
                { x: 2007, y: 424 },
                { x: 2008, y: 406 },
                { x: 2009, y: 393 },
                { x: 2010, y: 341 },
                { x: 2011, y: 337 },
                { x: 2012, y: 282 },
                { x: 2013, y: 311 },
                { x: 2014, y: 280 },
                { x: 2015, y: 220 },
                { x: 2016, y: 224 },
                { x: 2017, y: 151 },
                { x: 2018, y: 131 },
                { x: 2019, y: 65 },
                { x: 2020, y: 58 },
            ],
            borderColor: "#8ecdc8",
            borderColorName: "mint",
            borderWidth: 4,
            spanGaps: true,
            fill: false
        },
    ],
    options: {
        rangeX: [2006, 2022],
        rangeY: [0, 500]
    }
};
