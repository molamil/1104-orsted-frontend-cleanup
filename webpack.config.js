const path = require('path')
const webpack = require('webpack')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin');
const HtmlWebPackPlugin = require("html-webpack-plugin")
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin")
const HtmlWebpackExternalsPlugin = require('html-webpack-externals-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin');


const sites = [
    'page-counter',
    'page-sustainability-graphs',
]

const pageEntries = {}
const pagePlugins = []
sites.forEach((site, index) => {
    // create pageEntries
    pageEntries[`${sites[index]}/index`] = `./src/${sites[index]}/index.js`

    // create pagePlugins
    pagePlugins.push(
        new HtmlWebPackPlugin({
            template: `./src/${sites[index]}/index.html`,
            filename: `./${sites[index]}/index.html`,
            inject: 'head',
            chunks: [`${sites[index]}/index`],
        }),
    )
})


module.exports = (env, argv) => {
    // Seems the out-commented libraries are already included in the vendor bundle
    let externals = [
        {
            module: 'jquery',
            entry: 'https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js',
            global: 'jQuery',
        },
        {
            module: 'gsap',
            entry: 'https://cdnjs.cloudflare.com/ajax/libs/gsap/3.7.0/gsap.min.js',
            global: 'gsap',
        },
        {
            module: 'chart.js',
            entry: 'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js',
            global: 'Chart',
        },

    ]

    if (argv.mode === 'development') {
        // code taken from: https://orstedcdn.azureedge.net/Assets_/dist/css/vendor.css?d=0728c8a74d44133de6c69d75fb79a4757492c32675d017574f0f99dfd22782c1
        externals.push({
            module: 'src',
            entry: {
                path: 'shared/styles/orsted_com/vendor.css',
                cwpPatternConfig: {
                    context: path.resolve(__dirname, '.'),
                },
            },
        })
        // code taken from: https://orstedcdn.azureedge.net/Assets_/dist/css/app.css?d=539993f4fa3bec94c046b3de1f4b9a6c92006145ae72cfd855314e4f4e4899b2
        externals.push({
            module: 'src',
            entry: {
                path: 'shared/styles/orsted_com/app.css',
                cwpPatternConfig: {
                    context: path.resolve(__dirname, '.'),
                },
            },
        })

        externals.push({
            module: 'src',
            entry: {
                path: 'shared/js/global-helpers/development-only.js',
                cwpPatternConfig: {
                    context: path.resolve(__dirname, '.'),
                },
            },
        })
    }

    return {
        devtool: 'source-map',
        devServer: {
            open: true,
            hot: true,
            // https: true,
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader",
                    },
                },
                {
                    test: /\.html$/,
                    use: [{
                        loader: "html-loader",
                        options: {
                            minimize: false,
                        },
                    }],
                },
                {
                    test: /\.(sa|sc|c)ss$/,
                    use: [
                        argv.mode === 'development' ? 'style-loader' : MiniCssExtractPlugin.loader,
                        'css-loader',
                        'postcss-loader',
                        'sass-loader',
                    ],
                },
            ],
        },
        optimization: {
            minimize: true,
            minimizer: [
                new TerserPlugin({
                        cache: true,
                        parallel: true,
                        sourceMap: true,
                }),
                new OptimizeCSSAssetsPlugin({}),
            ]
        },
        entry: {
            index: './src/index.js',
            ...pageEntries,
        },
        plugins: [
            new CleanWebpackPlugin(['dist']),
            new webpack.HotModuleReplacementPlugin(),
            new CopyWebpackPlugin([
                { from: '-', to: '-' },
            ]),
            new HtmlWebPackPlugin({
                template: "./src/index.html",
                filename: "./index.html",
                inject: 'head',
                chunks: ['index'],
            }),
            ...pagePlugins,
            new MiniCssExtractPlugin({
                filename: "[name]_styles.css",
                chunkFilename: "[id].css",
            }),
            new HtmlWebpackExternalsPlugin({
                externals: externals,
            }),
        ],
    }
}
