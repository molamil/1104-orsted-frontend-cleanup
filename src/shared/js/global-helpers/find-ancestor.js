function findAncestor(el = null, selector) {
    if (el.closest) {
        return el.closest(selector);
    }
    // IE9+ polyfill
    if (!Element.prototype.matches) {
        Element.prototype.matches = Element.prototype.msMatchesSelector ||
                                    Element.prototype.webkitMatchesSelector;
    }
    var matches = el.matches || el.matchesSelector;
    while (el) {
        if (matches && matches.call(el, selector)) {
            return el;
        }
        el = el.parentNode;
    }
    return null;
}

export default findAncestor