export default {
    chartTitle: 'chart_title_energy_source',
    chartType: 'customBar',
    hero: {
        label: 'hero_1',
        positionOffset: {
            top: 0,
            right: 0,
        },
    },
    labelY: 'percent',
    labels: [
        '2006',
        '2008',
        '2010',
        '2012',
        '2014',
        '2016',
        '2018',
        '2019',
        '2020*',
    ],
    datasets: [
        {
            label: 'offshore_wind',
            data: [
                0.04 * 100,
                0.05 * 100,
                0.09 * 100,
                0.13 * 100,
                0.21 * 100,
                0.26 * 100,
                0.39 * 100,
                0.42 * 100,
                0.47 * 100,
            ],
            backgroundColor: '#3A9CDE',
            backgroundColorName: 'mint',
            borderWidth: 0,
            barPercentage: 0.65, // 0.71 on mobile...
            categoryPercentage: 1,
        },
        {
            label: 'onshore_wind',
            data: [
                0 * 100,
                0 * 100,
                0 * 100,
                0 * 100,
                0 * 100,
                0 * 100,
                0.02 * 100,
                0.13 * 100,
                0.18 * 100,
            ],
            backgroundColor: '#1073B5',
            backgroundColorName: 'mint',
            borderWidth: 0,
            barPercentage: 0.65, // 0.71 on mobile...
            categoryPercentage: 1,
        },
        {
            label: 'biomass',
            data: [
                0.08 * 100,
                0.11 * 100,
                0.16 * 100,
                0.19 * 100,
                0.22 * 100,
                0.24 * 100,
                0.34 * 100,
                0.31 * 100,
                0.24 * 100,
            ],
            backgroundColor: '#8ECDC8',
            backgroundColorName: 'mint',
            borderWidth: 0,
            barPercentage: 0.65, // 0.71 on mobile...
            categoryPercentage: 1,
        },
        {
            label: 'hydro',
            data: [
                0.02 * 100,
                0.03 * 100,
                0.03 * 100,
                0.03 * 100,
                0 * 100,
                0 * 100,
                0 * 100,
                0 * 100,
                0 * 100,
            ],
            backgroundColor: 'rgba(142, 205, 200, 0.6)',
            backgroundColorName: 'mint',
            borderWidth: 0,
            barPercentage: 0.65, // 0.71 on mobile...
            categoryPercentage: 1,
        },
        {
            label: 'renewable_waste',
            data: [
                0.03 * 100,
                0.03 * 100,
                0.02 * 100,
                0.02 * 100,
                0.01 * 100,
                0 * 100,
                0 * 100,
                0 * 100,
                0 * 100,
            ],
            // backgroundColor: 'rgba(100, 76, 118, 0.2)',
            // backgroundColorName: 'aubergine',
            backgroundColor: '#E5E8EB',
            backgroundColorName: 'mint',
            borderWidth: 0,
            barPercentage: 0.65, // 0.71 on mobile...
            categoryPercentage: 1,
        },
        {
            label: 'coal',
            data: [
                0.54 * 100,
                0.49 * 100,
                0.41 * 100,
                0.37 * 100,
                0.39 * 100,
                0.3 * 100,
                0.17 * 100,
                0.09 * 100,
                7,
            ],
            backgroundColor: '#644C76',
            backgroundColorName: 'aubergine',
            borderWidth: 0,
            barPercentage: 0.65, // 0.71 on mobile...
            categoryPercentage: 1,
        },
        {
            label: 'gas',
            data: [
                0.21 * 100,
                0.22 * 100,
                0.25 * 100,
                0.23 * 100,
                0.16 * 100,
                0.19 * 100,
                0.08 * 100,
                0.05 * 100,
                0.03 * 100,
            ],
            backgroundColor: 'rgba(100, 76, 118, 0.8)',
            backgroundColorName: 'aubergine',
            borderWidth: 0,
            barPercentage: 0.65, // 0.71 on mobile...
            categoryPercentage: 1,
        },
        {
            label: 'oil',
            data: [
                0.06 * 100,
                0.05 * 100,
                0.02 * 100,
                0.01 * 100,
                0 * 100,
                0.01 * 100,
                0 * 100,
                0 * 100,
                0 * 100,
            ],
            backgroundColor: '#99A4AE',
            backgroundColorName: 'aubergine',
            borderWidth: 0,
            barPercentage: 0.65, // 0.71 on mobile...
            categoryPercentage: 1,
        },
        {
            label: 'fossil_waste',
            data: [
                0.02 * 100,
                0.02 * 100,
                0.02 * 100,
                0.02 * 100,
                0.01 * 100,
                0 * 100,
                0 * 100,
                0 * 100,
                0 * 100,
            ],
            backgroundColor: '#3B4956',
            backgroundColorName: '',
            borderWidth: 0,
            barPercentage: 0.65, // 0.71 on mobile...
            categoryPercentage: 1,
        },
    ],
    options: {
        barStacked: true,
        dataStartIndex: 5,
        rangeY: [0, 100],
        layout: {
            legend: {
                position: "left-bottom",
                // listType: "row"
            }
        },
    }
}
