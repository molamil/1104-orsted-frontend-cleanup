const range = function(min, max, stepSize = 1) {
    return {
        scales: {
            xAxes: [
                {
                    type: 'linear',
                    ticks: {
                        autoSkip: false,
                        min,
                        max,
                        stepSize
                    }
                }
            ],
        },
    }
}

export default range
