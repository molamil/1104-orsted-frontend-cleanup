import _ from 'lodash'
import ua from './userAgent'

function throttle(func, limit) {
    let lastFunc;
    let lastRan;
    return function () {
        let context = this;
        let args = arguments;
        if (!lastRan) {
            func.apply(context, args);
            lastRan = Date.now();
        } else {
            clearTimeout(lastFunc);
            lastFunc = setTimeout(function () {
                if ((Date.now() - lastRan) >= limit) {
                    func.apply(context, args);
                    lastRan = Date.now();
                }
            }, limit - (Date.now() - lastRan))
        }
    }
}

function debounce(func, wait, immediate) {
    let timeout;
    return function () {
        let context = this, args = arguments;
        let later = function () {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        let callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};

function lerp(v0, v1, t) {
    return v0*(1-t)+v1*t
}
function clamp(v, start = 0, end = 1) {
    return Math.max(start, Math.min(end, v))
}
function mapRange(input_start, input_end, output_start, output_end, input) {
    return output_start + ((output_end - output_start) / (input_end - input_start)) * (input - input_start)
}


function isMobile() {
    if (_.get(ua(), 'mobile', false)) {
        return true
    }
    return false
}
function isDesktop() {
    if (_.get(ua(), 'desktop', false)) {
        return true
    }
    return false
}


export {
    throttle,
    debounce,
    lerp,
    clamp,
    mapRange,
    isMobile,
    isDesktop,
}
